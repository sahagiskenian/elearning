<?php

namespace Database\Factories\Head;

use App\Models\Head\Program;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProgramFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Program::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            "program_name"=> $this->faker->name()
            ,"program_description"=>$this->faker->paragraph($nbSentences = 3, $variableNbSentences = true)
            ,"program_url"=> $this->faker->imageUrl($width = 640, $height = 480)
            ,"program_link"=>$this->faker->url()
            ,"program_cost"=> $this->faker->numberBetween(100,1000)
            ,"program_time"=>$this->faker->numberBetween(60,6000)
            ,"featured"=>$this->faker->numberBetween(0,1)
            ,"program_start_date"=>$this->faker->date()
            ,"subcategory_id"=>$this->faker->numberBetween(1,20)

        ];
    }
}
