<?php

namespace Database\Factories\Head;

use App\Models\Head\Subcategory;
use Illuminate\Database\Eloquent\Factories\Factory;

class SubcategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Subcategory::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'subcategory_name' => $this->faker->name(),
            'subcategory_description' => $this->faker->paragraph($nbSentences = 3, $variableNbSentences = true),
            'subcategory_url' => $this->faker->imageUrl($width = 640, $height = 480) ,
            'category_id'=> $this->faker->numberBetween(1,10)
        ];
    }
}
