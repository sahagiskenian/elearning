<?php

namespace Database\Factories\Head;

use App\Models\Head\Organization;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrganizationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Organization::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "organization_name" => $this->faker->name() ,
            "organization_description"=> $this->faker->paragraph($nbSentences = 3, $variableNbSentences = true),
            "organization_url"=>  $this->faker->imageUrl($width = 640, $height = 480) ,
            "organization_link"=> $this->faker->url()  ,
            "featured" =>  $this->faker->numberBetween(0,1) ,
            "assign_date"=>   $this->faker->dateTime() ,
        ];
    }
}
