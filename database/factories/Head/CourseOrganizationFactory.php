<?php

namespace Database\Factories\Head;

use App\Models\Head\CourseOrganization;
use Illuminate\Database\Eloquent\Factories\Factory;

class CourseOrganizationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CourseOrganization::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            "course_id"=>$this->faker->numberBetween(1,20),
            "organization_id"=>$this->faker->numberBetween(1,20)
        ];
    }
}
