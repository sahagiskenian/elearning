<?php

namespace Database\Factories\Head;

use App\Models\Head\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

class CategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Category::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'category_name' => $this->faker->name(),
            'category_description' => $this->faker->paragraph($nbSentences = 3, $variableNbSentences = true),
            'category_url' => $this->faker->imageUrl($width = 640, $height = 480) ,

        ];
    }
}
