<?php

namespace Database\Factories\Head;

use App\Models\Head\Course;
use Illuminate\Database\Eloquent\Factories\Factory;

class CourseFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Course::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "course_name" =>$this->faker->name()
            ,"course_description" => $this->faker->paragraph($nbSentences = 3, $variableNbSentences = true)
            ,"course_url" =>$this->faker->imageUrl($width = 640, $height = 480)
            ,"course_link" =>$this->faker->url()
            ,"course_cost" =>$this->faker->numberBetween(100,1000),
            "course_time" =>$this->faker->numberBetween(60,6000)
            ,"featured" =>$this->faker->numberBetween(0,1)
            ,"course_start_date" =>$this->faker->date()
            ,"program_id" =>$this->faker->numberBetween(1,10)


        ];
    }
}
