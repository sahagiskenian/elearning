<?php

namespace Database\Factories\Head;

use App\Models\Head\ProgramOrganization;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProgramOrganizationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProgramOrganization::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            "program_id"=>$this->faker->numberBetween(1,20),
            "organization_id"=>$this->faker->numberBetween(1,20)
        ];
    }
}
