<?php

namespace Database\Seeders;

use App\Models\Head\Category;
use App\Models\Head\Course;
use App\Models\Head\CourseOrganization;
use App\Models\Head\Organization;
use App\Models\Head\Program;
use App\Models\Head\ProgramOrganization;
use App\Models\Head\Subcategory;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
       //  Category::factory(10)->create();
       // Subcategory::factory(10)->create();
      // Organization::factory(20)->create();
    //  Program::factory(10)->create();
    //  Course::factory(10)->create();
    CourseOrganization::factory(20)->create();
    ProgramOrganization::factory(20)->create();
    }
}
