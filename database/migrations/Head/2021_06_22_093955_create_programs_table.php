<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programs', function (Blueprint $table) {
            $table->id();
            $table->string('program_name')->nullable();
            $table->longText('program_description')->nullable();
            $table->longText('program_url')->nullable();
            $table->longText('program_link')->nullable();
            $table->integer('program_cost')->nullable();
            $table->string('program_time')->nullable();
            $table->date('program_start_date')->nullable();
            $table->boolean('featured')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programs');
    }
}
