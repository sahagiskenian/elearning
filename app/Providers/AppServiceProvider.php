<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->loadMigrationsFrom([

        //    database_path('migrations/Head'),
        //   database_path('migrations/Course'),
        //    database_path('migrations/Exam'),
         //   database_path('migrations/User'),
            database_path('migrations/Foreign')

        ]);
    }
}
