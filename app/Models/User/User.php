<?php

namespace App\Models\User;

use App\Models\Course\Question;
use App\Models\Course\Review;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'email',
        'password',
        'last_name',
        'type',
        'profile_image'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function reviews(){
        return $this->hasMany(Review::class);
    }
    public function userPrograms(){
        return $this->hasMany(UserProgram::class);
    }
    public function userCourses(){
        return $this->hasMany(UserCourse::class);
    }
    public function questions(){
        return $this->hasMany(Question::class);
    }
    public function userLectures(){
        return $this->hasMany(UserLecture::class);
    }
    public function userSessions(){
        return $this->hasMany(UserSession::class);
    }
    public function userAssignments(){
        return $this->hasMany(UserAssignment::class);
    }
    public function userExam(){
        return $this->hasMany(UserExam::class);
    }
}
