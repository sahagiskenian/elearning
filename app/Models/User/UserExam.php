<?php

namespace App\Models\User;

use App\Models\Exam\FinalExam;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserExam extends Model
{
    use HasFactory;

    use HasFactory;

    protected $fillable=["user_id","final_exam_id","mark","success","attempt_number"];




    public function user(){
        return $this->belongsTo(User::class);
    }
    public function exam(){
        return $this->belongsTo(FinalExam::class);
    }
}
