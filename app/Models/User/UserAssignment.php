<?php

namespace App\Models\User;

use App\Models\Exam\Assignment;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserAssignment extends Model
{
    use HasFactory;

    protected $fillable=["user_id","assignment_id","mark","begin","end","success","attempt_number"];




    public function user(){
        return $this->belongsTo(User::class);
    }
    public function assignment(){
        return $this->belongsTo(Assignment::class);
    }
}
