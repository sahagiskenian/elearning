<?php

namespace App\Models\User;

use App\Models\Head\Program;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserProgram extends Model
{
    use HasFactory;

    protected $fillable=["user_id","program_id","start_date","end_date","end_date_real","completed"];


    public function user(){
        return $this->belongsTo(User::class);
    }
    public function program(){
        return $this->belongsTo(Program::class);
    }
}
