<?php

namespace App\Models\Head;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProgramOrganization extends Model
{
    use HasFactory;
    protected $fillable=["program_id","organization_id"];




    public function program(){
        return $this->belongsTo(Program::class);
    }
    public function organization(){
        return $this->belongsTo(Organization::class);
    }
}
