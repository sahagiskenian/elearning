<?php

namespace App\Models\Head;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Organization extends Model
{
    use HasFactory,SoftDeletes;
    protected $fillable=["organization_name","organization_description","organization_url",
    "organization_link","featured","assign_date"
];




    public function organzationCourses(){
        return $this->hasMany(CourseOrganization::class);
    }
    public function programOrganization(){
        return $this->hasMany(ProgramOrganization::class);
    }
}
