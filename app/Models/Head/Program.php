<?php

namespace App\Models\Head;

use App\Models\User\UserProgram;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Program extends Model
{
    use HasFactory,SoftDeletes;
    protected $fillable=["program_name","program_description","program_url",
    "program_link","program_cost","program_time","featured","program_start_date","category_id","subcategory_id"
];




    public function category(){
        return $this->belongsTo(Category::class);
    }
    public function subcategory(){
        return $this->belongsTo(Subcategory::class);
    }
    public function courses(){
        return $this->hasMany(Course::class);
    }
    public function userPrograms(){
        return $this->hasMany(UserProgram::class);
    }
    public function programOrganization(){
        return $this->hasMany(ProgramOrganization::class);
    }
}
