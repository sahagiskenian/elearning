<?php

namespace App\Models\Head;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseOrganization extends Model
{
    use HasFactory;

    protected $fillable=["course_id","organization_id"];




    public function course(){
        return $this->belongsTo(Course::class);
    }
    public function organization(){
        return $this->belongsTo(Organization::class);
    }
}
