<?php

namespace App\Models\Head;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subcategory extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable=["subcategory_name","subcategory_description","subcategory_url","category_id"];




    public function category(){
        return $this->belongsTo(Category::class);
    }
    public function programs(){
        return $this->hasMany(Program::class);
    }
    public function courses(){
        return $this->hasMany(Course::class);
    }
}
