<?php

namespace App\Models\Head;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use HasFactory,SoftDeletes;
    protected $fillable=["category_name","category_description","category_url"];




    public function subcategories(){
        return $this->hasMany(Subcategory::class);
    }
    public function programs(){
        return $this->hasMany(Program::class);
    }
    public function courses(){
        return $this->hasMany(Course::class);
    }
}
