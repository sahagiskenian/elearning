<?php

namespace App\Models\Head;

use App\Models\Course\Mainpoint;
use App\Models\Course\Review;
use App\Models\Course\Session;
use App\Models\Exam\FinalExam;
use App\Models\User\UserCourse;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;
    protected $fillable=["course_name","course_description","course_url",
    "course_link","course_cost","course_time","featured",
    "course_start_date","category_id","subcategory_id","program_id"
];




    public function category(){
        return $this->belongsTo(Category::class);
    }
    public function subcategory(){
        return $this->belongsTo(Subcategory::class);
    }
    public function program(){
        return $this->belongsTo(Program::class);
    }
    public function mainpoints(){
        return $this->hasMany(Mainpoint::class);
    }
    public function reviews(){
        return $this->hasMany(Review::class);
    }
    public function userCourses(){
        return $this->hasMany(UserCourse::class);
    }
    public function sessions(){
        return $this->hasMany(Session::class);
    }
    public function finalexams(){
        return $this->hasMany(FinalExam::class);
    }
    public function courseOrgranizations(){
        return $this->hasMany(CourseOrganization::class);
    }
}
