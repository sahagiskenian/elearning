<?php

namespace App\Models\Course;

use App\Models\Head\Course;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mainpoint extends Model
{
    use HasFactory;

    protected $fillable=["point_title","point_description","point_order","course_id"];



    public function course(){
        return $this->belongsTo(Course::class);
    }
}
