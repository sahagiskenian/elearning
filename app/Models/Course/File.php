<?php

namespace App\Models\Course;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    use HasFactory;
    protected $fillable=["file_title","file_description","file_type","file_url","lecture_id"];



    public function lecture(){
        return $this->belongsTo(Lecture::class);
    }
}
