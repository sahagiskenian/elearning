<?php

namespace App\Models\Course;

use App\Models\Exam\AssignmentQuestion;
use App\Models\Head\Course;
use App\Models\User\UserSession;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    use HasFactory;
    protected $fillable=["session_title","session_description","session_order","course_id"];

    public function lectures(){
        return $this->hasMany(Lecture::class);
    }
    public function assignments(){
        return $this->hasMany(AssignmentQuestion::class);
    }
    public function userSessions(){
        return $this->hasMany(UserSession::class);
    }
    public function course(){
        return $this->belongsTo(Course::class);
    }


}
