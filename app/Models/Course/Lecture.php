<?php

namespace App\Models\Course;

use App\Models\User\UserLecture;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lecture extends Model
{
    use HasFactory;

    protected $fillable=["lecture_title","lecture_description","lecture_type","lecture_order","session_id"];



    public function session(){
        return $this->belongsTo(Session::class);
    }
    public function questions(){
        return $this->hasMany(Question::class);
    }
    public function userLectures(){
        return $this->hasMany(UserLecture::class);
    }
    public function files(){
        return $this->hasMany(File::class);
    }
}
