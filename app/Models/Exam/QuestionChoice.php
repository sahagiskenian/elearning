<?php

namespace App\Models\Exam;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuestionChoice extends Model
{
    use HasFactory;
    protected $table = 'assignment_question_choices';
    protected $fillable=["choice","assignment_question_id"];



    public function assignmentQuestion(){
        return $this->belongsTo(AssignmentQuestion::class);
    }


}
