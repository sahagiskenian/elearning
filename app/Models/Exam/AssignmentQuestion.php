<?php

namespace App\Models\Exam;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssignmentQuestion extends Model
{
    use HasFactory;
    protected $fillable=["question","answer","mark","question_type","assignment_id","final_exam_id"];





    public function finalexam(){
        return $this->belongsTo(FinalExam::class);
    }
    public function questionchoices(){
        return $this->hasMany(QuestionChoice::class);
    }
    public function assignment(){
        return $this->belongsTo(Assignment::class);
    }
}
