<?php

namespace App\Models\Exam;

use App\Models\Course\Question;
use App\Models\Course\Session;
use App\Models\User\UserAssignment;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    use HasFactory;
    protected $fillable=["assignment_name","assignment_mark","assignment_date","assignment_time","session_id"];





    public function userAssignments(){
        return $this->hasMany(UserAssignment::class);
    }
    public function questions(){
        return $this->hasMany(Question::class);
    }
    public function session(){
        return $this->belongsTo(Session::class);
    }
}
