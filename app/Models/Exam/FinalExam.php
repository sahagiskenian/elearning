<?php

namespace App\Models\Exam;

use App\Models\Head\Course;
use App\Models\User\UserExam;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FinalExam extends Model
{
    use HasFactory;

    protected $fillable=["exam_mark","exam_name","exam_title","exam_date","course_id"];


    public function questions(){
        return $this->hasMany(AssignmentQuestion::class);
    }
    public function userExams(){
        return $this->hasMany(UserExam::class);
    }
    public function course(){
        return $this->belongsTo(Course::class);
    }

}
